import geb.navigator.Navigator
import geb.spock.GebReportingSpec
import pages.DetailPage
import pages.EditPage
import pages.ListPage
import pages.AddPage


class OtherCrudSpecs extends GebReportingSpec{


    def "Clicking in add button"(){
        given :
            ListPage listPage = to ListPage

        when : "searching for a world"
            listPage.addButton.click()

        then: "Going to add page"
            AddPage addPage = to AddPage
            addPage.titleAdd.text() == "Cadastrar Pessoa"

    }

    def "Add a person"(){
        given :
            ListPage listPage = to ListPage
            String lastId = listPage.$('.p-table-row').collect {it.getAttribute('id')}.last()
            listPage.addButton.click()

        and:
            AddPage addPage = to AddPage

        when:
            addPage.nameInput.value("Ricardo Rodrigues")
            addPage.cpfInput.value("000.000.021.01")
            addPage.ageInput.value("22")
            addPage.saveButton.click()
        then:
            String data = lastId.toInteger() + 1;
            DetailPage detailPage = to DetailPage, data.toString()
            detailPage.nameValue.text() == "Ricardo Rodrigues"
            detailPage.cpfValue.text() == "000.000.021.01"
            detailPage.ageValue.text() == "22"
            detailPage.idValue.text() == data

    }

    def "Edit a person" (){
        given:
        ListPage listPage = to ListPage

        when:
            Navigator lastRow
            lastRow = listPage.$('.p-table-row').last()
            String editedId = lastRow.getAttribute('id')
            lastRow.click()

        and:
            DetailPage detailPage = to DetailPage, editedId
            detailPage.editButton.click()

        and:
            EditPage editPage = to EditPage, editedId
            editPage.nameInput.value("Teste 1")
            editPage.cpfInput.value("210.000.111.21")
            editPage.ageInput.value("21")
            editPage.saveButton.click()

        then:
            DetailPage detailPage1 = to DetailPage, editedId
            detailPage1.nameValue.text() == "Teste 1"
            detailPage1.cpfValue.text() == "210.000.111.21"
            detailPage1.ageValue.text() == "21"
            detailPage1.idValue.text() == editedId
    }

    def "Remove a person" (){
        given:
            ListPage listPage = to ListPage

        when:
            Navigator lastRow
            lastRow = listPage.$('.p-table-row').last()
            String deletedId = lastRow.getAttribute('id')
            lastRow.click()

        and:
            DetailPage detailPage = to DetailPage, deletedId
            detailPage.deleteButton.click()

        then:
            ListPage listPage1 = to ListPage

            if(!listPage1.$('empty-list-div').isDisplayed()){
                String[] list = listPage1.$('.p-table-row').collect {it.getAttribute('id')}
                !list.contains(deletedId)
            }
    }

}