package pages;


import geb.Page


class ListPage extends Page{
    static url="https://angular-crud-test-ffa326fa.herokuapp.com";
    static at={
        addButton.isDisplayed()
    };
    static content={
        addButton {$("#add-button")}
    };


}